void main(List<String> argument) {
  var operators = [];
  var postfix = [];
  // var argument = ['4', '^', '2', '+', '5', '*', '(', '2', '+', '1', ')'];

  for (int i = 0; i < argument.length; i++) {
    if (int.tryParse(argument[i]) != null) {
      postfix.add(argument[i]);
    }
    if (int.tryParse(argument[i]) == null &&
        argument[i] != '(' &&
        argument[i] != ')') {
      while (operators.isNotEmpty &&
          operators.last != '(' &&
          (argument[i] == '+' ||
              argument[i] == '-' &&
                  operators.last != '*' &&
                  operators.last != '/' &&
                  argument[i] == '*' ||
              argument[i] == '/' && operators.last != ')')) {
        postfix.add(operators.removeLast());

        break;
      }
      operators.add(argument[i]);
    }
    if (argument[i] == '(') {
      operators.add(argument[i]);
    }
    if (argument[i] == ')') {
      while (operators.last != '(') {
        postfix.add(operators.removeLast());
      }
      operators.removeLast();
    }
  }

  while (operators.isNotEmpty) {
    postfix.add(operators.removeLast());
  }

  print(postfix);
}
