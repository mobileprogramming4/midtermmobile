void main(List<String> arguments) {
  var values = [];

  for (int i = 0; i < arguments.length; i++) {
    if (int.tryParse(arguments[i]) != null) {
      values.add(int.parse(arguments[i]));
    } else {
      var right = values.removeLast();
      var left = values.removeLast();
      switch (arguments[i]) {
        case '+':
          var res = right + left;
          values.add(res.toString());
          break;
        case '-':
          var res = right - left;
          values.add(res.toString());
          break;
        case '*':
          var res = right * left;
          values.add(res.toString());
          break;
        case '/':
          var res = right / left;
          values.add(res.toString());
          break;
      }
    }
  }
  print(values);
}
